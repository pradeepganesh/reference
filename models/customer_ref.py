#-*- coding : utf-8 -*-

from odoo import time
from odoo import api, fields, models, _
from datetime import datetime
import psycopg2
import gc
import sys


class ResPartner(models.Model):
    _inherit = "res.partner"
    reffered_by = fields.Many2one('res.partner',string='Referrence')
    direct_refference_count = fields.Integer(compute='_compute_reference_count', string="Ref Count")	
    direct_refference_revenue = fields.Float(compute='_compute_reference_ref',String='Direct Referrence Revenue')
    leve_ref_count = fields.Integer(compute='_compute_has_children',string='Level Referrence Count')
    
    
    @api.multi
    @api.depends('reffered_by')    
    def _compute_reference_count(self):        
        read_group_result = self.env['res.partner'].read_group([('reffered_by', 'in', self.ids)], ['reffered_by'], ['reffered_by'])
        read_group_result_2 = self.env['res.partner'].read_group([('reffered_by', 'in', self.ids)], ['reffered_by','total_invoiced'], ['reffered_by', 'total_invoiced'])
        result = dict((data['reffered_by'][0],data['reffered_by_count']) for data in read_group_result)
        for partner in self:
            partner.direct_refference_count = result.get(partner.id, 0)
            
             
    
                
    @api.multi
    @api.depends('reffered_by')    
    def _compute_reference_ref(self):  
        list_name=[]      
        for i in self:            
            self.direct_refference_revenue = i.total_invoiced
    
    @api.multi
    @api.depends('reffered_by')       
    def _compute_has_children( root, x, data = None): 
        self.key = data  
        self.child = [] 
        # initialize the numChildren as 0 
        numChildren = 0
  
        if (root == None): 
           return 0
  
        #Creating a queue and appending the root 
        q = [] 
        q.append(root) 
  
        while (len(q) > 0) : 
            n = len(q) 
  
            # If this node has children 
            while (n > 0):  
  
                # Dequeue an item from queue and 
                # check if it is equal to x 
                # If YES, then return number of children 
                p = q[0] 
                q.pop(0) 
                if (p.key == x) : 
                   numChildren = numChildren + len(p.child) 
                   return numChildren 
              
                i = 0
              
                # Enqueue all children of the dequeued item 
                while ( i < len(p.child)): 
                    q.append(p.child[i]) 
                    i = i + 1
                n = n - 1
  
        return numChildren 
        print (numchildren)
    
    
    
    
    
    
    
            
    # @api.model
    # @api.multi
    # @api.depends('reffered_by')
    # def _compute_has_children(self):
        # list_name=[]
        # count=0
        # query = """
            # select * from res_partner
            # """
    
        # self.env.cr.execute(query)
        # list_name= self.env.cr.fetchall()        
        # res_list = [item for list2 in list_name for item in list2] 
        # print(len(gc.get_referrers(list_name)))
        # print(sys.getrefcount(list_name))
    
        
# class ResPartners(models.Model):
    # _inherit = 'res.partner'
    # def __init__(self, t, left=None, right=None):
        # self.root_value = t
        # self.left = left
        # self.right = right
        
    # def size(self):
        # count = 1
        # if self.left:
            # count += self.left.size()
        # if self.right:
            # count += self.right.size()
        # return count
        # print (count)
        
        
        # children = {}
        # for p, c in relations:
            # children.setdefault(p, []).append(c)
            # roots = set(children) - set(c for cc in children.values() for c in cc)    
            
    # @api.multi        
    # @api.depends('reffered_by')       
    # def _compute_has_children(self, cr, uid, ids, reffered_by, arg, context=None):
        # print ("WElcomw")
        # res = {}
        # for lead in self.browse(cr, uid, ids, context=context):
            # val = 0.0
            # for line in lead.reffered_by:
                # val += line.leve_ref_count
            # res[lead.id]= val
            # print ("HAI")
            # print (res)        
    
    # @api.multi
    # def _compute_reference_revenue(self):
        # read_group_result = self.env['res.partner'].read_group([('reffered_by', 'in', self.ids)], ['direct_refference_revenue'], ['direct_refference_revenue'])
        # result = dict((data['direct_refference_revenue'][0], data['referrence_by_revenue']) for data in read_group_result)
        # for partner in self:
            # partner.direct_refference_revenue = result.get(partner.id, 0)


# class customerreferrencedata(models.Model):
    # _name = 'customer.ref.data'
    
    
    # customer_name = fields.Char(string='Customer Name')
    # reffered_by =fields.Many2one('res.partner',string='Referrence')
    # direct_refference_count = fields.Integer(string='Direct Referrence Count',onchange='cus_fucntion')
    # direct_refference_revenue = fields.Float(String='Direct Referrence Revenue')
    # level_refference_count = fields.Integer(string='Level Referrence Count')
    # level_revenue = fields.Float(string='Level Revenue')
    
    
    
    # @api.model
    # @api.multi
    # @api.onchange('reffered_by')
    # def cus_fucntion(self):
        # list_name=[]
        # count=0
        # query = """
            # select customer_name from customer_ref_data where reffered_by =%d
            # """%self.reffered_by.id
    
        # self.env.cr.execute(query)
        # list_name= self.env.cr.fetchall()        
        # res_list = [item for list2 in list_name for item in list2] 
        # count =len(res_list)

        # values = {
            # 'direct_refference_count': count or False,
            
        # }
        # self.update(values)
